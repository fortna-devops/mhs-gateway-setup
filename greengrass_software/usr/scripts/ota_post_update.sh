#!/usr/bin/env bash
set -e

OLD_PID=$(pstree -p -s $$ | perl -n -e'/ggc-ota\((\d+)\)/ && print $1')
NEW_PID=$(ps -xo 'pid= command=' | awk 'match($0, /-p ('${OLD_PID}')/){print $1}')
systemd-notify --pid=${NEW_PID}
