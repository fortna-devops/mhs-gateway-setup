#!/usr/bin/env bash
set -e

SCRIPTS_DIR="$(pwd)/scripts"

tpm2_setup () {
    echo "**************"
    echo "* Setup TPM2 *"
    echo "**************"

    ${SCRIPTS_DIR}/tpm2_setup.sh
}

grub_setup () {
    echo "**************"
    echo "* Setup grub *"
    echo "**************"

    ${SCRIPTS_DIR}/grub_setup.sh
}

security_setup () {
    echo "******************"
    echo "* Setup security *"
    echo "******************"

    ${SCRIPTS_DIR}/security_setup.sh
}

greengrass_setup() {
    echo "********************"
    echo "* Setup greengrass *"
    echo "********************"

    ${SCRIPTS_DIR}/greengrass_setup.sh
}

audit_setup() {
    echo "***************"
    echo "* Setup audit *"
    echo "***************"

    ${SCRIPTS_DIR}/audit_setup.sh
}

case $1 in
    "tpm2")
        tpm2_setup
        ;;
    "grub")
        grub_setup
        ;;
    "security")
        security_setup
        ;;
    "greengrass")
        greengrass_setup
        ;;
    "audit")
        audit_setup
        ;;
    "all")
        tpm2_setup
        grub_setup
        security_setup
        greengrass_setup
        audit_setup
        ;;
    *)
        echo "Setup step names: tpm2,grub,security,greengrass,audit,all"
esac
