#!/usr/bin/env bash
set -e

SCRIPTS_DIR="$(pwd)/scripts"

if [[ ${EUID} != 0 ]]; then
    echo "sudo required"
    exit 1
fi

echo "Install monit"

apt -y install monit

echo "Mail server configuration"

read -e -p "Enter mail server: " -i "smtp.gmail.com" MAIL_SERVER

read -e -p "Enter mail server port: " -i "587" MAIL_SERVER_PORT

read -e -p "Enter mail server user name: " -i "predict.notification.system@gmail.com" MAIL_SERVER_USER_NAME

read -p "Enter mail server password: " MAIL_SERVER_PASSWORD

read -e -p "Enter mail list (space separated): " -i "Dmitry.Platon@dataart.com Igor.Panteleyev@dataart.com" MAIL_LIST

MAILS=""
MAIL_LIST=(${MAIL_LIST})
for MAIL in "${MAIL_LIST[@]}"; do
    MAILS="set alert ${MAIL} NOT ON { action, instance, pid, ppid, nonexist }\n${MAILS}"
done

MONIT_CONFIG=$(cat ${SCRIPTS_DIR}/monitrc)
MONIT_CONFIG=$(echo "${MONIT_CONFIG//\{\{MAIL_SERVER\}\}/${MAIL_SERVER}}")
MONIT_CONFIG=$(echo "${MONIT_CONFIG//\{\{MAIL_SERVER_PORT\}\}/${MAIL_SERVER_PORT}}")
MONIT_CONFIG=$(echo "${MONIT_CONFIG//\{\{MAIL_SERVER_USER_NAME\}\}/${MAIL_SERVER_USER_NAME}}")
MONIT_CONFIG=$(echo "${MONIT_CONFIG//\{\{MAIL_SERVER_PASSWORD\}\}/${MAIL_SERVER_PASSWORD}}")
MONIT_CONFIG=$(echo -e "${MONIT_CONFIG//\{\{MAILS\}\}/${MAILS}}")

echo "${MONIT_CONFIG}" > /etc/monit/conf.d/monitrc

service monit restart
