#!/bin/sh -e
if [ "$1" = "prereqs" ]; then exit 0; fi
. /usr/share/initramfs-tools/hook-functions
copy_exec /usr/local/bin/tpm2_unseal /bin/
cp -fpL /usr/local/lib/libtss* ${DESTDIR}/lib/x86_64-linux-gnu/
