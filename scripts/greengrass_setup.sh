#!/usr/bin/env bash
set -e

SCRIPTS_DIR="$(pwd)/scripts"
RSA_DIR="$(pwd)/rsa"

# Getting current user home dir
# https://unix.stackexchange.com/questions/438564/how-do-ubuntu-and-debian-manage-home-for-users-with-sudo-privileges
# https://superuser.com/questions/484277/get-home-directory-by-username
USER_HOME=$(eval echo ~${USER})
USER_SSH_DIR="${USER_HOME}/.ssh"

GGC_DIR="$(pwd)/greengrass_software"
GGC_REQUIREMENTS=${GGC_DIR}/requirements.txt
GGC_USR_SCRIPTS=${GGC_DIR}/usr
GGC_ARCHIVE=${GGC_DIR}/greengrass.tar.gz

GGC_CACHE_DIR="/var/cache/gateway_cache"
GGC_ROOT_DIR="/opt/greengrass/"
GGC_CERTS_DIR=${GGC_ROOT_DIR}/certs

GGC_USER="ggc_user"
GGC_GROUP="ggc_group"
GGC_TTY_ACCESS_GROUP="dialout"

if [[ ${EUID} != 0 ]]; then
    echo "sudo required"
    exit 1
fi

echo "Install rsa key"
mkdir -p ${USER_SSH_DIR}
cp ${RSA_DIR}/* ${USER_SSH_DIR}/
chmod 600 ${USER_SSH_DIR}/id_rsa

echo "User configuration"

adduser --system ${GGC_USER}
addgroup --system ${GGC_GROUP}
usermod -aG ${GGC_TTY_ACCESS_GROUP} ${GGC_USER}

echo "Install dependencies"

# May 23, 2019
# python3.7 doesn't really has distutils in ubuntu repositories
# python3.7-distutils is a virtual package
# instead of this python3.6-distutils will be installed (with all dependencies)
# it's the only way to make it works properly for now
# python - will be pointed to python3.7
# python3 - will be pointed to python3.6
apt update && apt install -y sqlite3 python3.7 python3.7-dev python3.7-distutils git
update-alternatives --install /usr/bin/python python /usr/bin/python3.7 1
curl -sL https://bootstrap.pypa.io/get-pip.py | python -
ssh-keyscan -H bitbucket.org >> ${USER_SSH_DIR}/known_hosts
pip install -r ${GGC_REQUIREMENTS}

echo "Install greengrass core"

mkdir -p /opt
rm -rf ${GGC_ROOT_DIR}
tar -oxf ${GGC_ARCHIVE} -C /opt/
wget -T 30 -q -O ${GGC_CERTS_DIR}/root.ca.pem "https://www.amazontrust.com/repository/AmazonRootCA1.pem"
chown -R ${GGC_USER}:${GGC_GROUP} ${GGC_CERTS_DIR}
cp -r ${GGC_USR_SCRIPTS} ${GGC_ROOT_DIR}


mkdir -p ${GGC_CACHE_DIR}
chown -R ${GGC_USER}:${GGC_GROUP} ${GGC_CACHE_DIR}

cp ${SCRIPTS_DIR}/greengrass.service /etc/systemd/system/greengrass.service
cp ${SCRIPTS_DIR}/greengrass_ota.service /etc/systemd/system/greengrass_ota.service

systemctl enable greengrass.service
systemctl enable greengrass_ota.service
