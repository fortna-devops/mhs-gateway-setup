#!/usr/bin/env bash
set -e

SCRIPTS_DIR="$(pwd)/scripts"

if [[ ${EUID} != 0 ]]; then
    echo "sudo required"
    exit 1
fi

echo "Restrict core dumps"

if ! grep -q "* hard core 0" /etc/security/limits.conf; then
    echo "* hard core 0" >> /etc/security/limits.conf
fi

if ! grep -q "root hard core 0" /etc/security/limits.conf; then
    echo "root hard core 0" >> /etc/security/limits.conf
fi

if ! grep -q "fs.suid_dumpable = 0" /etc/sysctl.conf; then
    echo "fs.suid_dumpable = 0" >> /etc/sysctl.conf
fi

if ! grep -q "DumpCore=no" /etc/systemd/system.conf; then
    echo "DumpCore=no" >> /etc/systemd/system.conf
fi

sysctl -w fs.suid_dumpable=0

echo "Restrict access to 'su' command"

if ! grep -q "auth required pam_wheel.so use_uid" /etc/pam.d/su; then
    echo "auth required pam_wheel.so use_uid" >> /etc/pam.d/su
fi

read -e -p "Enter user name: " -i "mhs-predict" USER_NAME

sed -i '/wheel:.*/d' /etc/group
echo "wheel:x:10:root,${USER_NAME}" >> /etc/group

echo "Protect SSH server"

SSHD_CONFIG=$(cat ${SCRIPTS_DIR}/sshd_config)

echo "${SSHD_CONFIG//\{\{USER_NAMES\}\}/${USER_NAME}}" > /etc/ssh/sshd_config

chown root:root /etc/ssh/sshd_config
chmod og-rwx /etc/ssh/sshd_config

echo "Disable extra FS support"

cp ${SCRIPTS_DIR}/CIS.conf /etc/modprobe.d/CIS.conf

if ! grep -q "tmpfs /dev/shm tmpfs defaults,nodev,nosuid,noexec 0 0" /etc/fstab; then
    echo "tmpfs /dev/shm tmpfs defaults,nodev,nosuid,noexec 0 0" >> /etc/fstab
fi
