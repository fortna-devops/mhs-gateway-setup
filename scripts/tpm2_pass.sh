#!/bin/sh
set -e

echo "Unlocking via TPM" >&2
export TPM2TOOLS_TCTI="device:/dev/tpm0"

# temporary workaround for case when TPM chip is not ran on first boot
if [ ! -e /dev/tpm0 ]; then
    tpm2_unseal -H 0x81000100 -L sha1:0,2,3,7 >&2 || true
    echo "/dev/tpm0 not found! Rebooting..." >&2
    sleep 10
    reboot
fi

tpm2_unseal -H 0x81000100 -L sha1:0,2,3,7

if [ $? -eq 0 ]; then
    exit
fi

/lib/cryptsetup/askpass "Unlocking the disk fallback $CRYPTTAB_SOURCE ($CRYPTTAB_NAME)\nEnter passphrase: "
