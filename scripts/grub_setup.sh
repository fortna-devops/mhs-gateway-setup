#!/usr/bin/env bash
set -e

SCRIPTS_DIR="$(pwd)/scripts"

if [[ ${EUID} != 0 ]]; then
    echo "sudo required"
    exit 1
fi

sed -i 's/--class os"/--class os --unrestricted"/g' /etc/grub.d/10_linux

echo "Setup grub password"

GRUB_PASSWORD_HASH=$(grub-mkpasswd-pbkdf2 | tee /dev/tty | grep -Po "grub.*")

HEAD=$(head -n 5 /etc/grub.d/40_custom)

echo "${HEAD}" > /etc/grub.d/40_custom
echo "set superusers=\"mhs\"" >> /etc/grub.d/40_custom
echo "password_pbkdf2 mhs ${GRUB_PASSWORD_HASH}" >> /etc/grub.d/40_custom

echo "Update grub password"

update-grub
