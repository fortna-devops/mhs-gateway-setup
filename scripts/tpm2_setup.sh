#!/usr/bin/env bash
set -e

SCRIPTS_DIR="$(pwd)/scripts"
TPM2_DIR="${HOME}/tpm2"
TPM2_TSS_VERSION="2.0.0"
TPM2_ABRMD_VERSION="2.0.0"
TPM2_TOOLS_VERSION="3.1.0"

if [[ ${EUID} != 0 ]]; then
    echo "sudo required"
    exit 1
fi

read -p "Enter the TPM password: " TPM_PASSWORD

if [ -z "${TPM_PASSWORD}" ]; then
    echo "TPM password required"
    exit 1
fi

read -e -p "Enter the main partition: " -i "sda3" TPM_PARTITION

if [ -z "${TPM_PARTITION}" ]; then
    echo "Main partition required"
    exit 1
fi

echo "Install TPM2 software"

apt -y install autoconf-archive libcmocka0 libcmocka-dev net-tools \
build-essential git pkg-config gcc g++ m4 libtool automake libgcrypt20-dev \
libssl-dev uthash-dev autoconf libglib2.0-dev libdbus-1-dev libsapi-dev \
libcurl4-gnutls-dev

rm -rf $TPM2_DIR && mkdir -p $TPM2_DIR && cd $TPM2_DIR

git clone https://github.com/tpm2-software/tpm2-tss.git
cd tpm2-tss
git checkout ${TPM2_TSS_VERSION}
./bootstrap
./configure --with-udevrulesdir=/etc/udev/rules.d
make -j8
make install
udevadm control --reload-rules && udevadm trigger
ldconfig
cd $TPM2_DIR

git clone https://github.com/tpm2-software/tpm2-abrmd.git
cd tpm2-abrmd
git checkout ${TPM2_ABRMD_VERSION}
if [ -z $(getent passwd tss) ]; then
    useradd --system --user-group tss
fi
./bootstrap
./configure --with-dbuspolicydir=/etc/dbus-1/system.d
make -j8
make install
pkill -HUP dbus-daemon
systemctl daemon-reload
cd $TPM2_DIR

git clone https://github.com/tpm2-software/tpm2-tools.git
cd tpm2-tools
git checkout ${TPM2_TOOLS_VERSION}
./bootstrap
./configure
make -j8
sudo make install
cd $TPM2_DIR

echo "Generate secret key"

dd bs=1 count=32 if=/dev/urandom of=/secret_key.bin
cryptsetup luksAddKey /dev/${TPM_PARTITION} /secret_key.bin

echo "Configure TPM2"

tpm2_takeownership -o ${TPM_PASSWORD}
tpm2_takeownership -c
tpm2_pcrlist -L sha1:0,2,3,7 -o pcrs.bin
tpm2_createpolicy -P -L sha1:0,2,3,7 -F pcrs.bin -f policy.digest
tpm2_createprimary -H e -g sha1 -G rsa -C primary.context
tpm2_create -g sha256 -G keyedhash -u obj.pub -r obj.priv -c primary.context \
            -L policy.digest -A "noda|adminwithpolicy|fixedparent|fixedtpm" \
            -I /secret_key.bin
tpm2_load -c primary.context -u obj.pub -r obj.priv -C load.context
tpm2_evictcontrol -A o -H 0x80000100 -S 0x81000100 -c load.context

echo "Update initramfs"

cp ${SCRIPTS_DIR}/tpm2_pass.sh /tpm2_pass.sh
cp ${SCRIPTS_DIR}/tpm2.sh /etc/initramfs-tools/hooks/tpm2.sh

chmod +x /tpm2_pass.sh
chmod +x /etc/initramfs-tools/hooks/tpm2.sh

if [ ! -f /etc/crypttab.backup ]; then
    cp /etc/crypttab /etc/crypttab.backup
fi
UUID=$(blkid -o value -s UUID /dev/${TPM_PARTITION})
echo "${TPM_PARTITION}_crypt UUID=${UUID} none luks,keyscript=/tpm2_pass.sh" > /etc/crypttab

update-initramfs -u
