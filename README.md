# MHS gateway setup
This repository contains scripts for automation gateway setup.

For now it has 5 independent steps:

- TPM2 setup
- GRUB setup
- Security setup
- AWS Greengrass setup
- Audit setup

# Usage

For now it is recommended to run each step separately.

```sh
sudo ./setup.sh
Setup step names: tpm2,grub,security,greengrass,audit,all
```
